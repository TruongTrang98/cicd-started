FROM node:14-alpine3.12

WORKDIR /app

COPY . .

RUN npm install && npm install -g pm2

CMD ["pm2-runtime", "ecosystem.config.js"]